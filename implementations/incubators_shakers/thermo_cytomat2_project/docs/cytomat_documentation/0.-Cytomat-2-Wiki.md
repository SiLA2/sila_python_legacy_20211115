**Cytomat 2**

*  This wiki summarizes the documentation of the Cytomat-2 incubator
    *  The documentation seems to be much newer than the firmware of the incubator
    *  Some commands are not working or not even installed. These commands are marked in the [Sequence-Instructions](3. Sequence-Instructions) chapters
    *  There is no [Swap-Station](1. Terms) installed. The [Transfer-Station](1. Terms) has only one [Plate](1. Terms) and is NOT movable
    *  No Barcode Function is installed
    *  The [CO2-Feed](1. Terms) or [Heating-System](1. Terms) functions are not installed or the connection to it is disrupted, since some commands are working.
    *  There is no X-Axis motor
*  The wiki contains a chapter for explanation of the most important [Terms](1. Terms)
*  The documentation consists of three important chapters
1.    The different [States](2. State Query) of the [PSS](1. Terms) which are saved in registers and can be queried
2.    The [Sequence-Instructions](3. Sequence-Instructions) which contain the most important commands that can be executed
3.    The [Error-Handling](4. Error-Handling) routines that take action if [Error-Handling](4. Error-Handling) for the different [Sequence-Instructions](3. Sequence-Instructions) are activated
