/**
 * @fileoverview gRPC-Web generated client stub for sila2.de.unigreifswald.example.temperatureprovider.v0
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');


var SiLAFramework_pb = require('./SiLAFramework_pb.js')
const proto = {};
proto.sila2 = {};
proto.sila2.de = {};
proto.sila2.de.unigreifswald = {};
proto.sila2.de.unigreifswald.example = {};
proto.sila2.de.unigreifswald.example.temperatureprovider = {};
proto.sila2.de.unigreifswald.example.temperatureprovider.v0 = require('./TemperatureProvider_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProviderClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProviderPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProviderClient} The delegate callback based client
   */
  this.delegateClient_ = new proto.sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProviderClient(
      hostname, credentials, options);

};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Parameters,
 *   !proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Responses>}
 */
const methodInfo_TemperatureProvider_GetTemperature = new grpc.web.AbstractClientBase.MethodInfo(
  proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Responses,
  /** @param {!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Parameters} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Responses.deserializeBinary
);


/**
 * @param {!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Parameters} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Responses)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Responses>|undefined}
 *     The XHR Node Readable Stream
 */
proto.sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProviderClient.prototype.getTemperature =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProvider/GetTemperature',
      request,
      metadata || {},
      methodInfo_TemperatureProvider_GetTemperature,
      callback);
};


/**
 * @param {!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Parameters} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.GetTemperature_Responses>}
 *     The XHR Node Readable Stream
 */
proto.sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProviderPromiseClient.prototype.getTemperature =
    function(request, metadata) {
  var _this = this;
  return new Promise(function (resolve, reject) {
    _this.delegateClient_.getTemperature(
      request, metadata, function (error, response) {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Parameters,
 *   !proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Responses>}
 */
const methodInfo_TemperatureProvider_Get_CurrentTemperatureUnint = new grpc.web.AbstractClientBase.MethodInfo(
  proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Responses,
  /** @param {!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Parameters} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Responses.deserializeBinary
);


/**
 * @param {!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Parameters} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Responses)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Responses>|undefined}
 *     The XHR Node Readable Stream
 */
proto.sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProviderClient.prototype.get_CurrentTemperatureUnint =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProvider/Get_CurrentTemperatureUnint',
      request,
      metadata || {},
      methodInfo_TemperatureProvider_Get_CurrentTemperatureUnint,
      callback);
};


/**
 * @param {!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Parameters} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.sila2.de.unigreifswald.example.temperatureprovider.v0.Get_CurrentTemperatureUnint_Responses>}
 *     The XHR Node Readable Stream
 */
proto.sila2.de.unigreifswald.example.temperatureprovider.v0.TemperatureProviderPromiseClient.prototype.get_CurrentTemperatureUnint =
    function(request, metadata) {
  var _this = this;
  return new Promise(function (resolve, reject) {
    _this.delegateClient_.get_CurrentTemperatureUnint(
      request, metadata, function (error, response) {
        error ? reject(error) : resolve(response);
      });
  });
};


module.exports = proto.sila2.de.unigreifswald.example.temperatureprovider.v0;

