# HelloSiLA2

This example implements a HelloSiLA2 client. It is a minimal example of client/server communication.

In a (virtual) environment where the sila2lib is installed it can directly be started from the shell. First start the server, then the client.   
The current client implementation will call a number of standard features, provoke an error, and then continue to run the servers commands first in simulation, then in real mode (see [client implementation](./HelloSiLA2_client.py)).

There are two versions available:
**HelloSiLA2_minimal** : the absolute bare minimum SiLA 2 Server / Client
**HelloSiLA2_full**    : a bit more instructive version with server infos and error handling  

## Running HelloSiLA2_full

To start the Server, run:   
`python HelloSiLA2_server.py`

Make sure to wait until the server is fully initialized and prints `Server 'HelloSiLA2' started!`. Once the server is started, you can use a new terminal to start the client:  
`python HelloSiLA2_client.py`

## Reference Output

The output of the server should be along the following lines:

**Server:**

```
> python HelloSiLA2_server.py
DEBUG   | config.read_config_file: Reading config from C:\Users\Timm\AppData\Roaming\SiLA2\HelloSiLA2\HelloSiLA2.conf
DEBUG   | SiLAService.__init__: Initialising org.silastandard feature: SiLAService
DEBUG   | SimulationController.__init__: Initialising org.silastandard feature: SimulationController.
INFO    | sila_server.__init__: Starting server without any encryption.
INFO    | HelloSiLA2_server.__init__: Starting SiLA2 server with server name: HelloSiLA2
DEBUG   | GreetingProvider_simulation.__init__: Started server in mode: Simulation
DEBUG   | SimulationController.StartSimulationMode: StartSimulationMode()
DEBUG   | GreetingProvider_simulation.__init__: Started server in mode: Simulation
DEBUG   | sila_service_detection.registerService: UUID: 04be2f12-0d54-4e14-ba86-ce5f7e5ed76d
INFO    | sila_service_detection.registerService: registratering the SiLA2 service HelloSiLA2 in the network...127.0.0.1 with port 50051
INFO    | sila_service_detection.registerService: Registration done ...
Server 'HelloSiLA2' started!
Please press ctrl-c to stop...
```

**Client:**

```
DEBUG   | config.read_config_file: Reading config from C:\Users\Timm\AppData\Roaming\SiLA2\HelloSiLA2Client\HelloSiLA2Client.conf
INFO    | sila_client.__init__: Overwriting given hostname "localhost" with IP address "127.0.0.1" since IP takes precedence.
INFO    | HelloSiLA2_client.__init__: Starting SiLA2 service client for service HelloSiLA2 with service name: HelloSiLA2Client
DEBUG   | HelloSiLA2_client.run: Display name: HelloSiLA2
DEBUG   | HelloSiLA2_client.run: Description: This is Hello World SiLA2 test service
DEBUG   | HelloSiLA2_client.run: Version: 0.1.1
INFO    | HelloSiLA2_client.<module>: Connected to SiLA Server HelloSiLA2 running in version 0.1.1.
Service description: This is Hello World SiLA2 test service
INFO    | HelloSiLA2_client.<module>: Running in Simulation mode:
DEBUG   | sila_client.switchToSimMode: Switching to simulation mode.
DEBUG   | HelloSiLA2_client.SayHello: SayHello response: Greeting {
  value: "Hello SiLA client!"
}

DEBUG   | HelloSiLA2_client.SayHello: SayHello response: Greeting {
  value: "Hello SiLA client!"
}

DEBUG   | HelloSiLA2_client.Get_StartYear: Reading unobservable property StartYear:
DEBUG   | HelloSiLA2_client.Get_StartYear: Get_StartYear response: StartYear {
  value: 2000
}

INFO    | HelloSiLA2_client.<module>: Running in Real mode:
DEBUG   | sila_client.switchToRealMode: Switching to real mode.
DEBUG   | HelloSiLA2_client.SayHello: SayHello response: Greeting {
  value: "Hello John Doe!"
}

DEBUG   | HelloSiLA2_client.SayHello: SayHello response: Greeting {
  value: "Hello Your Name!"
}

DEBUG   | HelloSiLA2_client.Get_StartYear: Reading unobservable property StartYear:
DEBUG   | HelloSiLA2_client.Get_StartYear: Get_StartYear response: StartYear {
  value: 2019
}
```
